<?php

namespace App\Jobs;

use App\Services\SmsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $smsService;
    protected $message;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param SmsService $smsService
     * @param $message
     * @param $user
     */
    public function __construct(SmsService $smsService, $message, $user)
    {
        $this->smsService = $smsService;
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->smsService->sendSMS($this->message, $this->user->phone);
    }
}
