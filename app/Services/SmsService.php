<?php


namespace App\Services;


use Exception;
use Illuminate\Support\Facades\Http;

class SmsService
{

    protected $api_key;

    /**
     * SmsService constructor.
     * @param $api_key
     * @param $url
     */
    public function __construct()
    {
        $this->api_key = env('FATSMS_API_KEY');
    }

    public function sendSMS($message, $phone, $url = 'https://fatsms.com/send-sms') {
        try {
            $response = Http::asForm()->post($url, [
                'to_phone' => $phone,
                'message' => $message,
                'api_key' => $this->api_key
            ]);
        } catch (Exception $e) {
            return [
                'error' => $e
            ];
        }
        return $response->json();
    }


}
