<?php


namespace App\Services;


use App\Models\User;
use App\Models\UserAccessKey;
use Illuminate\Support\Str;

/**
 * Class UserAccessKeyService
 * @package App\Services
 */
class UserAccessKeyService
{
    protected $userAccesKey;

    /**
     * UserAccessKeyService constructor.
     * @param UserAccessKey $userAccesKey
     */
    public function __construct(UserAccessKey $userAccesKey)
    {
        $this->userAccesKey = $userAccesKey;
    }


    /**
     * @param User $user
     * @param int $quantity
     */
    public function createAccessKeys(User $user, int $quantity = 1): void
    {
        $range = range(100000, 999999);
        shuffle($range);
        $numbers = array_slice($range, 0, $quantity);
        foreach ($numbers as $number) {
            UserAccessKey::create([
                'access_key' => $number,
                'user_id' => $user->id
            ]);
        }
    }

    /**
     * @param int $accessKey
     * @param int $user_id
     * @return bool
     */
    public function validateAccessKey(int $accessKey, int $user_id)
    {
        $response = $this->userAccesKey->where('user_id', $user_id)->where('in_use', true)->where('access_key', $accessKey)->first();
        if (!$response || $response->access_key !== $accessKey) {
            return false;
        }
        $response->delete();
        return !(!$response || $response->access_key !== $accessKey);

    }


    /**
     * @param $user_id
     * @return mixed
     */
    public function reserveKey (int $user_id) {
        $access_key = UserAccessKey::where('user_id', $user_id)->first();
        if (!$access_key) {
            return false;
        }
        $access_key->in_use = true;
        $access_key->save();
        return $access_key;

    }
}
