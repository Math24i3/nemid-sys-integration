<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NemIdController extends Controller
{


    public function index(Request $request) {
        $data = [
            'company' => $request->company,
            'loginEndpoint' => route('login')
        ];
        return view('nemID', $data);
    }
}
