<?php

namespace App\Http\Controllers;
use App\Jobs\SendSMS;
use App\Services\SmsService;
use App\Services\UserAccessKeyService;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Models\User;
use Validator;


class AuthController extends Controller
{

    protected $userAccessKeyService;
    protected $smsService;

    /**
     * Create a new AuthController instance.
     *
     * @param UserAccessKeyService $userAccessKeyService
     * @param SmsService $smsService
     */
    public function __construct(UserAccessKeyService $userAccessKeyService, SmsService $smsService) {
        $this->userAccessKeyService = $userAccessKeyService;
        $this->middleware('auth:api', ['except' => ['login', 'authenticate', 'register', 'verifyToken', 'resendKey']]);
        $this->smsService = $smsService;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string|between:4,40',
            'password' => 'required|string|min:4',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!auth()->attempt($validator->validated())) {
            return response()->json([
                'message' => 'Login failed. Please try again',
                'error' => 'Unauthorized'], 401);
        }
        if(!$user = auth()->user()) {
            return response()->json([
                'message' => 'Login failed. Please try again',
                'error' => 'Unauthorized'], 401);
        }

        $ak_response = $this->userAccessKeyService->reserveKey($user->id);
        if (!$ak_response) {
            return response()->json([
                'message' => 'You are out of sms keys. Contact provider to generate more',
                'error' => 'Unauthorized'
            ], 401);
        }
        // SMS service here
        $smsMessage = 'Hi ' . $user->name . ', your key is: ' . $ak_response->access_key;
        SendSMS::dispatch($this->smsService, $smsMessage, $user);
        //$smsResponse = $this->smsService->sendSMS($smsMessage, (int) $user->phone);

        return response()->json([
            'message' => 'Please provide the sms key in the next request',
            'keyRoute' => route('key_auth'),
            'resendKeyRoute' => route('resendKey'),
        ], 201);
    }

    public function authenticate (Request $request) {
        $validFields = $request->only([
            'user_id',
            'password',
            'user_access_key',
        ]);

        $validator = Validator::make($validFields, [
            'user_id' => 'required|string|between:4,40',
            'password' => 'required|string|min:4',
            'user_access_key' => 'required|string|min:6|max:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth()->attempt(['user_id' => $validFields['user_id'], 'password' => $validFields['password']])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if (!$this->userAccessKeyService->validateAccessKey($validFields['user_access_key'], auth()->user()->id)) {
            return response()->json(
                [
                    'error' => 'Key was wrong'
                ], 401);
        }
        return $this->createNewToken($token);
    }

    public function resendKey(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|string|between:4,40',
            'password' => 'required|string|min:4',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!auth()->attempt($validator->validated())) {
            return response()->json([
                'message' => 'Login failed. Please try again',
                'error' => 'Unauthorized'], 401);
        }


        if(!$user = auth()->user()) {
            return response()->json([
                'message' => 'User not authenticated',
                'error' => 'Unauthorized'], 401);
        }

        $ak_response = $this->userAccessKeyService->reserveKey($user->id);
        if (!$ak_response) {
            return response()->json([
                'message' => 'You are out of sms keys. Contact provider to generate more',
                'error' => 'Unauthorized'
            ], 401);
        }
        // SMS service here
        $smsMessage = 'Resend key is: ' . $ak_response->access_key;
        SendSMS::dispatch($this->smsService, $smsMessage, $user);
        //$this->smsService->sendSMS($smsMessage, (int) $user->phone);

        return response()->json([
            'message' => 'Key was resend',
            'route' => route('key_auth')
        ], 201);
    }

    /**
     * Register a User.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'user_id' => 'required|string|between:4,40|unique:users',
            'phone' => 'required|string|min:8|max:11',
            'password' => 'required|string|confirmed|min:4',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password)]
        ));

        if ($user) {
            $this->userAccessKeyService->createAccessKeys($user, 10);
        }

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function userProfile(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Validate authenticated User.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyToken(Request $request): JsonResponse
    {
        try {
            $decoded = JWT::decode($request->jwt, new Key(env('JWT_SECRET'), 'HS256'));
        } catch (Exception $e) {
            return response()->json([
                'decoded' => false,
                'error' => $e->getMessage()
            ],400);
        }
        return response()->json([
            'decoded' => $decoded
        ],200);
    }



    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function createNewToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'JWT',
            'refresh' => route('refresh'),
            'logout' => route('logout'),
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

}
