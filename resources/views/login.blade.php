<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NemID</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp"
        rel="stylesheet">
    <!-- Styles -->
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
        .card-body {
            background-image: "{{URL::asset('/images/nemID-background.jpg')}}";
            background-repeat: repeat-y, repeat-x;
        }
        .embed-responsive-1by1 {
            min-width: 250px; min-height: 425px;
        }
    </style>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
    <script defer src="{{ mix('js/app.js') }}"></script>
</head>
<body>
    <div id="app">
        <main class="py-3 container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card w-100 h-100">
                            <div class="card-header">{{ __('Login') }}</div>
                            <div class="card-body h-100" id="login-body-form" style="background: url({{URL::asset('/images/nemID-background.jpg')}}); height: 450px; border: 2px solid rgb(235, 240, 243);">
                                @csrf
                                <div class="row justify-content-between w-100 mb-4">
                                    <div class="col-md-7">
                                        <h3>To login to the application, please use your NemID</h3>
                                        <small>to monitor and start the queue worker, use these commands:</small>
                                        <small>inside the redis queue container: redis-cli monitor</small>
                                        <small>inside the fpm (application) container: php artisan queue:work</small>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="embed-responsive embed-responsive-1by1">
                                            <iframe id="auth-frame" class="float-right" allowtransparency="true" scrolling="no" frameborder="0" src="" style="min-width: 250px; max-width: 325px; height: 425px;"></iframe>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </main>

    </div>
    <script>
        let myIframe = document.getElementById("auth-frame");
        const url_string = "http://localhost:8080/nemid";
        const company = "Danske Bank"
        let adsURL = url_string+"?company="+company;
        myIframe.src = adsURL;

        const eventMethod = window.addEventListener
            ? "addEventListener"
            : "attachEvent";
        const eventer = window[eventMethod];
        const messageEvent = eventMethod === "attachEvent"
            ? "onmessage"
            : "message";

        eventer(messageEvent, function (e) {
            //if (e.origin !== 'http://the-trusted-iframe-origin.com') return;
            if (e.data && e.data.access_token) {
                console.log("Authorized!!: ", e.data);
            } else if (e.data === 401 || e.message === 401) {
                console.log("Unauthorized: ",e.data);
            }

        });
    </script>
</body>
</html>
